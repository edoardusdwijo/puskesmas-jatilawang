<?php
include("../config.php");
session_start();

if ($_SESSION['roleAktif'] != "PPN Only") {
	header("location:../");
	exit;
}

if (isset($_POST['editProfile'])) {
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $nip = $_POST['nip'];
    $tempatLahir = $_POST['tempatLahir'];
    $tanggalLahir = $_POST['tanggalLahir'];
    $pangkat = $_POST['pangkat'];
    $jabatan = $_POST['jabatan'];
    $role = $_POST['role'];
    $password = md5($_POST['password']);
    $username = $_POST['username'];

    $queryCekUsername = "SELECT * FROM tbl_user WHERE username='$username' AND id!=$id";
    $sqlCekUsername = mysqli_query($db, $queryCekUsername);

    if ($role == "KMP Full"){
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    }else if ($role == "UKM Full"){
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    }else if ($role == "UKPP Full"){
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    }else if ($role == "PPN Full"){
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    }else if ($role == "PMP Full"){
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    }else{
        $cekRole = 0;
    }

    if (mysqli_num_rows($sqlCekUsername) == 0 && $cekRole == 0) {
        if ($_POST['password'] == "") {
            $query = "UPDATE tbl_user SET nama='$nama', nip='$nip', tempatLahir='$tempatLahir', tanggalLahir='$tanggalLahir', pangkat='$pangkat', jabatan='$jabatan', username='$username', `role`='$role' WHERE id='$id'";
            $sql = mysqli_query($db, $query);

            if ($sql) {
                echo "
	        <script>
	            alert('DATA PROFIL BERHASIL DI PERBARUI');
				document.location.href = '../ppn-only';
	        </script>
	        ";
            } else {
                echo "
	        <script>
	            alert('DATA GAGAL DI PERBARUI');
				document.location.href = '../ppn-only';
	        </script>
	        ";
            }
        } else {
            $query = "UPDATE tbl_user SET nama='$nama', nip='$nip', tempatLahir='$tempatLahir', tanggalLahir='$tanggalLahir', pangkat='$pangkat', jabatan='$jabatan', `password`='$password', username='$username', `role`='$role' WHERE id='$id'";
            $sql = mysqli_query($db, $query);

            if ($sql) {
                echo "
	        <script>
	            alert('DATA PROFIL BERHASIL DI PERBARUI');
				document.location.href = '../ppn-only';
	        </script>
	        ";
            } else {
                echo "
	        <script>
	            alert('DATA GAGAL DI PERBARUI');
				document.location.href = '../ppn-only';
	        </script>
	        ";
            }
        }
    } else {
        if(mysqli_num_rows($sqlCekUsername) != 0){
            echo "
	        <script>
	            alert('MAAF USERNAME YANG ANDA MASUKKAN SUDAH ADA, HARAP GUNAKAN USERNAME LAIN');
				document.location.href = '../ppn-only';
	        </script>
	        ";
        }else{
            echo "
	        <script>
	            alert('MAAF ROLE ".$role." HANYA BOLEH 1 USER, SILAHKAN GUNAKAN ROLE LAIN ATAU UBAH PEMILIK ROLE ".$role." SEBELUMNYA DENGAN ROLE LAIN');
				document.location.href = '../ppn-only';
	        </script>
	        ";
        }
    }
}