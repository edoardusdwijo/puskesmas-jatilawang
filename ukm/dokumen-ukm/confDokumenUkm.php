<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "UKM Full") {
	header("location:../../");
	exit;
}

if (isset($_POST['editProfile'])) {
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $nip = $_POST['nip'];
    $tempatLahir = $_POST['tempatLahir'];
    $tanggalLahir = $_POST['tanggalLahir'];
    $pangkat = $_POST['pangkat'];
    $jabatan = $_POST['jabatan'];
    $role = $_POST['role'];
    $password = md5($_POST['password']);
    $username = $_POST['username'];

    $queryCekUsername = "SELECT * FROM tbl_user WHERE username='$username' AND id!=$id";
    $sqlCekUsername = mysqli_query($db, $queryCekUsername);

    if ($role == "KMP Full") {
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    } else if ($role == "UKM Full") {
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    } else if ($role == "UKPP Full") {
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    } else if ($role == "PPN Full") {
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    } else if ($role == "PMP Full") {
        $queryCekRole = "SELECT * FROM tbl_user WHERE `role`='$role' AND id!=$id";
        $sqlCekRole = mysqli_query($db, $queryCekRole);
        $cekRole = mysqli_num_rows($sqlCekRole);
    } else {
        $cekRole = 0;
    }

    if (mysqli_num_rows($sqlCekUsername) == 0 && $cekRole == 0) {
        if ($_POST['password'] == "") {
            $query = "UPDATE tbl_user SET nama='$nama', nip='$nip', tempatLahir='$tempatLahir', tanggalLahir='$tanggalLahir', pangkat='$pangkat', jabatan='$jabatan', username='$username', `role`='$role' WHERE id='$id'";
            $sql = mysqli_query($db, $query);

            if ($sql) {
                echo "
	        <script>
	            alert('DATA PROFIL BERHASIL DI PERBARUI');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
            } else {
                echo "
	        <script>
	            alert('DATA GAGAL DI PERBARUI');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
            }
        } else {
            $query = "UPDATE tbl_user SET nama='$nama', nip='$nip', tempatLahir='$tempatLahir', tanggalLahir='$tanggalLahir', pangkat='$pangkat', jabatan='$jabatan', `password`='$password', username='$username', `role`='$role' WHERE id='$id'";
            $sql = mysqli_query($db, $query);

            if ($sql) {
                echo "
	        <script>
	            alert('DATA PROFIL BERHASIL DI PERBARUI');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
            } else {
                echo "
	        <script>
	            alert('DATA GAGAL DI PERBARUI');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
            }
        }
    } else {
        if (mysqli_num_rows($sqlCekUsername) != 0) {
            echo "
	        <script>
	            alert('MAAF USERNAME YANG ANDA MASUKKAN SUDAH ADA, HARAP GUNAKAN USERNAME LAIN');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
        } else {
            echo "
	        <script>
	            alert('MAAF ROLE " . $role . " HANYA BOLEH 1 USER, SILAHKAN GUNAKAN ROLE LAIN ATAU UBAH PEMILIK ROLE " . $role . " SEBELUMNYA DENGAN ROLE LAIN');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
        }
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $kategori = $_POST['kategori'];
    $tipe = $_POST['tipe'];
    $namaFile = $_POST['namaFile'];

    $docsNama = $_FILES['docs']['name'];
    $docsTmp = $_FILES['docs']['tmp_name'];
    $docsSize = $_FILES['docs']['size'];
    $docsTipe = $_FILES['docs']['type'];

    if ($docsNama == "") {
        $query = "UPDATE tbl_ukm SET nama='$nama', kategori='$kategori', tipe='$tipe', waktu=current_timestamp() WHERE id='$id'";
        $sql = mysqli_query($db, $query);

        if ($sql) {
            echo "
	        <script>
	            alert('DATA BERHASIL DI PERBARUI');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
        } else {
            echo "
	        <script>
	            alert('DATA GAGAL DI PERBARUI');
				document.location.href = '../dokumen-ukm';
	        </script>
	        ";
        }
    } else {
        $docsNamaBaru = $id . $docsNama;
        $hapusDokumenLama = unlink('../../dokumen/ukm/' . $namaFile);
        if ($hapusDokumenLama) {
            $pindahFileUploadKeServer = move_uploaded_file($docsTmp, "../../dokumen/ukm/" . $docsNamaBaru);
            if ($pindahFileUploadKeServer) {
                $query = "UPDATE tbl_ukm SET nama='$nama', kategori='$kategori', tipe='$tipe', waktu=current_timestamp(), namaFile='$docsNamaBaru' WHERE id='$id'";
                $sql = mysqli_query($db, $query);
                if ($sql) {
                    echo "
                        <script>
                            alert('DATA BERHASIL DI PERBARUI');
                            document.location.href = '../dokumen-ukm';
                        </script>
                    ";
                } else {
                    echo "
                        <script>
                            alert('DATA GAGAL DI PERBARUI');
                            document.location.href = '../dokumen-ukm';
                        </script>
                    ";
                }
            }
        } else {
            echo "
                <script>
                    alert('GAGAL MENGHAPUS FILE YANG INGIN DI GANTI');
                    document.location.href = '../dokumen-ukm';
                </script>
            ";
        }
    }
}

if (isset($_POST['hapus'])) {
    $id = $_POST['id'];
    $namaFile = $_POST['namaFile'];

    $query = "DELETE FROM tbl_ukm WHERE id='$id'";
    $sql = mysqli_query($db, $query);
    $hapusDokumenLama = unlink('../../dokumen/ukm/' . $namaFile);
    if ($sql && $hapusDokumenLama) {
        echo "
            <script>
                alert('DATA BERHASIL DI HAPUS');
                document.location.href = '../dokumen-ukm';
            </script>
        ";
    }else{
        echo "
            <script>
                alert('DATA GAGAL DI HAPUS');
                document.location.href = '../dokumen-ukm';
            </script>
        ";
    }
}
