<?php
include("../config.php");
session_start();

if ($_SESSION['roleAktif'] != "KMP Only") {
    header("location:../");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../css/admin-dokumenkmp.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>KMP-Dokumen KMP</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<p>Kepemimpinan dan Manajemen Puskesmas</p>
		</div>
		<a href="./" class="active">Dokumen KMP</a>
		<a href="divisi-kmp/">Divisi KMP</a>
		<a href="../logout.php"><img src="../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<?php
		$queryProfil = "SELECT * FROM tbl_user WHERE id='" . $_SESSION['id'] . "'";
		$sqlProfil = mysqli_query($db, $queryProfil);
		$dataProfil = mysqli_fetch_array($sqlProfil);
		?>
		<div class="profile">
			<p class="text-end"><?php echo $dataProfil['nama'] ?>
				<button type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#editProfile">Edit</button>

			<div class="modal fade" id="editProfile" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="editModalLabel">Edit Profile</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<form action="confKmpOnly.php" method="POST">
							<div class="modal-body">
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Nama</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $dataProfil['nama'] ?>" name="nama">
									<input type="hidden" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $dataProfil['id'] ?>" name="id">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">NIP</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan NIP" value="<?php echo $dataProfil['nip'] ?>" name="nip">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tempat Lahir" value="<?php echo $dataProfil['tempatLahir'] ?>" name="tempatLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tanggal Lahir" value="<?php echo $dataProfil['tanggalLahir'] ?>" name="tanggalLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Pangkat</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Pangkat" value="<?php echo $dataProfil['pangkat'] ?>" name="pangkat">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Jabatan" value="<?php echo $dataProfil['jabatan'] ?>" name="jabatan">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Role</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" value="<?php echo $dataProfil['role'] ?>" name="role" readonly>
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Username</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Username" value="<?php echo $dataProfil['username'] ?>" name="username">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Password (KOSONGKAN JIKA TIDAK INGIN MENGGANTI PASSWORD)</label>
									<input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password" name="password">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary" value="editProfile" name="editProfile">Simpan Perubahan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			</p>
		</div>

		<div class="judul">
			<p>File Dokumen KMP</p>
		</div>

		<div class="tabelUser table-responsive mb-4">
			<table class="table table-hover table-light" id="tbl_user">
				<thead class="table-info">
					<tr>
						<th scope="col" class="text-center">Nama</th>
						<th scope="col" class="text-center">Kategori</th>
						<th scope="col" class="text-center">Tipe</th>
						<th scope="col" class="text-center">Waktu</th>
						<th scope="col" class="text-center">File</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_kmp";
					$query = mysqli_query($db, $sql);
					$no = 1;

					while ($data = mysqli_fetch_array($query)) {
					?>
						<tr>
							<td class='text-center'><?php echo $data['nama'] ?></td>
							<td class='text-center'><?php echo $data['kategori'] ?></td>
							<td class='text-center'><?php echo $data['tipe'] ?></td>
							<td class='text-center'><?php echo $data['waktu'] ?></td>
							<td class='text-center'>
								<a class="btn btn-primary btn-sm" href="../dokumen/kmp/<?php echo $data['namaFile'] ?>" target="_blank" role="button">Unduh Dokumen</a>
							</td>
						</tr>
					<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
		
	</div>
	<!-- end content -->

	<!-- start footer -->
	<div class="footer text-center text-lg-start fixed-bottom">
		<div class="text-center p-3">Copyright © <script>document.write(new Date().getFullYear())</script> Allrights reserved to Puskesmas Jatilawang
		</div>
	</div>
	<!-- end footer -->


</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>