<?php
    include("config.php");
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login PKM JTL</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/a81368914c.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<img class="wave" src="img/wave.png">
	<div class="container">
		<div class="img">
			<img src="img/bg.svg">
		</div>
		<div class="login-content">
			<form action="" method="POST">
				<img src="img/logo-bms.png">
				<h2 class="title">puskesmas jatilawang</h2>
           		<div class="input-div one">
           		   <div class="i">
           		   		<i class="fas fa-user"></i>
           		   </div>
           		   <div class="div">
           		   		<h5>Username</h5>
           		   		<input type="text" class="input" name="username">
           		   </div>
           		</div>
           		<div class="input-div pass">
           		   <div class="i"> 
           		    	<i class="fas fa-lock"></i>
           		   </div>
           		   <div class="div">
           		    	<h5>Password</h5>
           		    	<input type="password" class="input" name="password">
            	   </div>
            	</div>
            	<a href="#">Forgot Password?</a>
            	<input type="submit" class="btn" value="login" name="login">
            </form>
            <?php
                if (isset($_POST['login'])) {

                    // ambil data dari formulir
                    $username = $_POST['username'];
                    $password = md5($_POST['password']);
                
                    // buat query
                    $sql = "SELECT * FROM tbl_user WHERE username = '$username'";
                    $query = mysqli_query($db, $sql);
                    $data = mysqli_fetch_assoc($query);
                
                    // apakah query simpan berhasil?
                    if (mysqli_num_rows($query) > 0) {
                        if ($data['password'] == $password) {
                            if ($data['role'] == 'kmp_full') {
                                $_SESSION['roleAktif'] = "KMP Full";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI KMP FULL');
                                            document.location.href = 'kmp/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'kmp_only') {
                                $_SESSION['roleAktif'] = "KMP Only";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI KMP ONLY');
                                            document.location.href = 'kmp-only/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'ukm_full') {
                                $_SESSION['roleAktif'] = "UKM Full";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI UKM FULL');
                                            document.location.href = 'ukm/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'ukm_only') {
                                $_SESSION['roleAktif'] = "UKM Only";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI UKM ONLY');
                                            document.location.href = 'ukm-only/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'ukpp_full') {
                                $_SESSION['roleAktif'] = "UKPP Full";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI UKPP FULL');
                                            document.location.href = 'ukpp/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'ukpp_only') {
                                $_SESSION['roleAktif'] = "UKPP Only";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI UKPP ONLY');
                                            document.location.href = 'ukpp-only';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'ppn_full') {
                                $_SESSION['roleAktif'] = "PPN Full";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI PPN FULL');
                                            document.location.href = 'ppn/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'ppn_only') {
                                $_SESSION['roleAktif'] = "PPN Only";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI PPN ONLY');
                                            document.location.href = 'ppn-only/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'pmp_full') {
                                $_SESSION['roleAktif'] = "PMP Full";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI PMP FULL');
                                            document.location.href = 'pmp/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'pmp_only') {
                                $_SESSION['roleAktif'] = "PMP Only";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI PMP ONLY');
                                            document.location.href = 'pmp-only/';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'Kepegawaian') {
                                $_SESSION['roleAktif'] = "Kepegawaian";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI KEPEGAWAIAN');
                                            document.location.href = 'frameworks/kpgFull.php';
                                        </script>
                                        ";
                            } else if ($data['role'] == 'admin') {
                                $_SESSION['roleAktif'] = "admin";
                                $_SESSION['id'] = $data['id'];
                                echo "
                                        <script>
                                            alert('ANDA BERHASIL LOGIN SEBAGAI ADMIN');
                                            document.location.href = 'admin/';
                                        </script>
                                        ";
                            }else{
                                echo "
                                        <script>
                                            alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
                                            document.location.href = 'index.php';
                                        </script>
                                    ";
                            }
                        } else if (mysqli_num_rows($query) == 0) {
                            echo "
                                        <script>
                                            alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
                                            document.location.href = 'index.php';
                                        </script>
                                    ";
                        } else {
                            echo "
                                        <script>
                                            alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
                                            document.location.href = 'index.php';
                                        </script>
                                    ";
                        }
                    } else {
                        echo "
                                        <script>
                                            alert('USERNAME / PASSWORD SALAH SILAHKAN LOGIN KEMBALI');
                                            document.location.href = 'index.php';
                                        </script>
                                    ";
                    }
                }
            ?>
        </div>
    </div>
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>
