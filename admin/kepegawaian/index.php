<?php
include("../../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../../");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../../css/admin-kepegawaian.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Admin-Kepegawaian</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<p>Kepegawaian</p>
		</div>
		<a href="./" class="active">Data Kepegawaian</a>
		<a href="#dokumen-karyawan" data-bs-toggle="collapse">Dokumen Karyawan</a>
		<div class="collapse sub-menu" id="dokumen-karyawan">
			<a href="dokumen-karyawan/">Upload</a>
			<a href="dokumen-karyawan/lihat/">Lihat</a>
		</div>
		<a href="#surat-masuk" data-bs-toggle="collapse">Surat Masuk</a>
		<div class="collapse sub-menu" id="surat-masuk">
			<a href="surat-masuk/">Upload</a>
			<a href="surat-masuk/lihat/">Lihat</a>
		</div>
		<a href="#surat-keluar" data-bs-toggle="collapse">Surat Keluar</a>
		<div class="collapse sub-menu" id="surat-keluar">
			<a href="surat-keluar/">Upload</a>
			<a href="surat-keluar/lihat/">Lihat</a>
		</div>
		<a href="../">Kembali ke Halaman Admin</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<?php
		$queryProfil = "SELECT * FROM tbl_user WHERE id='" . $_SESSION['id'] . "'";
		$sqlProfil = mysqli_query($db, $queryProfil);
		$dataProfil = mysqli_fetch_array($sqlProfil);
		?>
		<div class="profile">
			<p class="text-end"><?php echo $dataProfil['nama'] ?>
				<button type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#editProfile">Edit</button>

			<div class="modal fade" id="editProfile" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="editModalLabel">Edit Profile</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<form action="confKpg.php" method="POST">
							<div class="modal-body">
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Nama</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $dataProfil['nama'] ?>" name="nama">
									<input type="hidden" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $dataProfil['id'] ?>" name="id">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">NIP</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan NIP" value="<?php echo $dataProfil['nip'] ?>" name="nip">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tempat Lahir" value="<?php echo $dataProfil['tempatLahir'] ?>" name="tempatLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tanggal Lahir" value="<?php echo $dataProfil['tanggalLahir'] ?>" name="tanggalLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Pangkat</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Pangkat" value="<?php echo $dataProfil['pangkat'] ?>" name="pangkat">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Jabatan" value="<?php echo $dataProfil['jabatan'] ?>" name="jabatan">
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Role</label>
									<select class="form-select" aria-label="Default select example" name="role" required>
										<option selected value="<?php echo $dataProfil['role'] ?>"><?php echo $dataProfil['role'] ?></option>
										<option value="KMP Full">KMP Full</option>
										<option value="KMP Only">KMP Only</option>
										<option value="UKM Full">UKM Full</option>
										<option value="UKM Only">UKM Only</option>
										<option value="UKPP Full">UKPP Full</option>
										<option value="UKPP Only">UKPP Only</option>
										<option value="PPN Full">PPN Full</option>
										<option value="PPN Only">PPN Only</option>
										<option value="PMP Full">PMP Full</option>
										<option value="PMP Only">PMP Only</option>
										<option value="Kepegawaian"> Kepegawaian</option>
										<option value="Admin">Admin</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Username</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Username" value="<?php echo $dataProfil['username'] ?>" name="username">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Password (KOSONGKAN JIKA TIDAK INGIN MENGGANTI PASSWORD)</label>
									<input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password" name="password">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary" value="editProfile" name="editProfile">Simpan Perubahan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			</p>
		</div>

		<div class="judul">
			<p>Data Kepegawaian</p>
		</div>
		<div class="tambah-data">
			<button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#validasiSurat">Tambah Data Karyawan</button>

			<div class="modal fade" id="validasiSurat" tabindex="-1" aria-labelledby="tambahModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tambahModalLabel">Tambah Data Karyawan</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<form action="confKpg.php" method="POST" enctype="multipart/form-data">
							<div class="modal-body">
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Nama</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" name="nama" placeholder="Masukkan Nama">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">NIP</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" name="nip" placeholder="Masukkan NIP">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" name="tempatLahir" placeholder="Masukkan Tempat Lahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" id="exampleFormControlInput1" name="tanggalLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Pangkat</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" name="pangkat" placeholder="Masukkan Pangkat">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" name="jabatan" placeholder="Masukkan Jabatan">
								</div>
								<!-- <div class="mb-3">
									<label for="recipient-name" class="col-form-label">Role</label>
									<select class="form-select" aria-label="Default select example" name="role" required>
										<option selected>Pilih Role</option>
										<option value="KMP Full">KMP Full</option>
										<option value="KMP Only">KMP Only</option>
										<option value="UKM Full">UKM Full</option>
										<option value="UKM Only">UKM Only</option>
										<option value="UKPP Full">UKPP Full</option>
										<option value="UKPP Only">UKPP Only</option>
										<option value="PPN Full">PPN Full</option>
										<option value="PPN Only">PPN Only</option>
										<option value="PMP Full">PMP Full</option>
										<option value="PMP Only">PMP Only</option>
										<option value="Kepegawaian"> Kepegawaian</option>
										<option value="Admin">Admin</option>
									</select>
								</div> -->
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary" value="save" name="save">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="tabelUser table-responsive">
			<table class="table table-hover table-light" id="tbl_user">
				<thead class="table-info">
					<tr>
						<th scope="col" class="text-center">Nama</th>
						<th scope="col" class="text-center">NIP</th>
						<th scope="col" class="text-center">Tempat Lahir</th>
						<th scope="col" class="text-center">Tanggal Lahir</th>
						<th scope="col" class="text-center">Pangkat</th>
						<th scope="col" class="text-center">Jabatan</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_user";
					$query = mysqli_query($db, $sql);
					$no = 1;

					while ($data = mysqli_fetch_array($query)) {
					?>
						<tr>
							<td class='text-center'><?php echo $data['nama'] ?></td>
							<td class='text-center'><?php echo $data['nip'] ?></td>
							<td class='text-center'><?php echo $data['tempatLahir'] ?></td>
							<td class='text-center'><?php echo $data['tanggalLahir'] ?></td>
							<td class='text-center'><?php echo $data['pangkat'] ?></td>
							<td class='text-center'><?php echo $data['jabatan'] ?></td>
						</tr>
					<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
		
	</div>
	<!-- end content -->

	<!-- start footer -->
	<div class="footer text-center text-lg-start fixed-bottom">
		<div class="text-center p-3">Copyright © <script>document.write(new Date().getFullYear())</script> Allrights reserved to Puskesmas Jatilawang
		</div>
	</div>
	<!-- end footer -->


</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>