<?php
include("../config.php");
session_start();

if ($_SESSION['roleAktif'] != "admin") {
	header("location:../");
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
	<link href='https://fonts.googleapis.com/css?family=Plus Jakarta Sans' rel='stylesheet'>
	<link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
	<link rel="stylesheet" href="../css/admin-managemenakun.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
	<title>Admin-Managemen Akun</title>
</head>

<body>
	<!-- start sidebar -->
	<div class="sidebar">
		<div class="logo mt-4 mb-4">
			<p>Admin Puskesmas</p>
		</div>
		<a class="active" href="./">Manajemen Akun</a>
		<a href="kmp/">KMP</a>
		<a href="ukm/">UKM</a>
		<a href="ukpp/">UKPP</a>
		<a href="ppn/">PPN</a>
		<a href="pmp/">PMP</a>
		<a href="kepegawaian/">Kepegawaian</a>
		<a href="../logout.php"><img src="../src/icon/icon-logout.png" alt="" class="icon">Logout</a>

	</div>
	<!-- end sidebar -->

	<!-- start content -->
	<div class="content">
		<?php
		$queryProfil = "SELECT * FROM tbl_user WHERE id='" . $_SESSION['id'] . "'";
		$sqlProfil = mysqli_query($db, $queryProfil);
		$dataProfil = mysqli_fetch_array($sqlProfil);
		?>
		<div class="profile">
			<p class="text-end"><?php echo $dataProfil['nama'] ?>
				<button type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#editProfile">Edit</button>

			<div class="modal fade" id="editProfile" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="editModalLabel">Edit Profile</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<form action="confAdmin.php" method="POST">
							<div class="modal-body">
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Nama</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $dataProfil['nama'] ?>" name="nama">
									<input type="hidden" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $dataProfil['id'] ?>" name="id">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">NIP</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan NIP" value="<?php echo $dataProfil['nip'] ?>" name="nip">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tempat Lahir" value="<?php echo $dataProfil['tempatLahir'] ?>" name="tempatLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tanggal Lahir" value="<?php echo $dataProfil['tanggalLahir'] ?>" name="tanggalLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Pangkat</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Pangkat" value="<?php echo $dataProfil['pangkat'] ?>" name="pangkat">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Jabatan" value="<?php echo $dataProfil['jabatan'] ?>" name="jabatan">
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Role</label>
									<select class="form-select" aria-label="Default select example" name="role" required>
										<option selected value="<?php echo $dataProfil['role'] ?>"><?php echo $dataProfil['role'] ?></option>
										<option value="KMP Full">KMP Full</option>
										<option value="KMP Only">KMP Only</option>
										<option value="UKM Full">UKM Full</option>
										<option value="UKM Only">UKM Only</option>
										<option value="UKPP Full">UKPP Full</option>
										<option value="UKPP Only">UKPP Only</option>
										<option value="PPN Full">PPN Full</option>
										<option value="PPN Only">PPN Only</option>
										<option value="PMP Full">PMP Full</option>
										<option value="PMP Only">PMP Only</option>
										<option value="Kepegawaian"> Kepegawaian</option>
										<option value="Admin">Admin</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Username</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Username" value="<?php echo $dataProfil['username'] ?>" name="username">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Password (KOSONGKAN JIKA TIDAK INGIN MENGGANTI PASSWORD)</label>
									<input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password" name="password">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary" value="editProfile" name="editProfile">Simpan Perubahan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			</p>
		</div>

		<div class="judul">
			<p>Manajemen Akun Karyawan Puskesmas</p>
		</div>
		<div class="tambah-data">
			<button type="button" class="btn btn-outline-primary btn-sm" data-bs-toggle="modal" data-bs-target="#tambahData">Tambah Data</button>

			<div class="modal fade" id="tambahData" tabindex="-1" aria-labelledby="tambahModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="tambahModalLabel">Tambah Data</h5>
							<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
						</div>
						<form action="confAdmin.php" method="POST">
							<div class="modal-body">
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Nama</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" name="nama">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">NIP</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan NIP" name="nip">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tempat Lahir" name="tempatLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
									<input type="date" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tanggal Lahir" name="tanggalLahir">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Pangkat</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Pangkat" name="pangkat">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Jabatan" name="jabatan">
								</div>
								<div class="mb-3">
									<label for="recipient-name" class="col-form-label">Role</label>
									<select class="form-select" aria-label="Default select example" name="role" required>
										<option selected disabled value="">Pilih Role</option>
										<option value="kmp_full">KMP Full</option>
										<option value="kmp_only">KMP Only</option>
										<option value="ukm_full">UKM Full</option>
										<option value="ukm_only">UKM Only</option>
										<option value="ukpp_full">UKPP Full</option>
										<option value="ukpp_only">UKPP Only</option>
										<option value="ppn_full">PPN Full</option>
										<option value="ppn_only">PPN Only</option>
										<option value="pmp_full">PMP Full</option>
										<option value="pmp_only">PMP Only</option>
										<option value="kepegawaian"> Kepegawaian</option>
										<option value="admin">Admin</option>
									</select>
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Username</label>
									<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Username" name="username">
								</div>
								<div class="mb-3">
									<label for="exampleFormControlInput1" class="form-label">Password</label>
									<input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password" name="password">
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary" value="save" name="save">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="tabelUser table-responsive">
			<table class="table table-hover table-light" id="tbl_user">
				<thead class="table-info">
					<tr>
						<th scope="col" class="text-center">Nama</th>
						<th scope="col" class="text-center">NIP</th>
						<th scope="col" class="text-center">Tempat Lahir</th>
						<th scope="col" class="text-center">Tanggal Lahir</th>
						<th scope="col" class="text-center">Pangkat</th>
						<th scope="col" class="text-center">Jabatan</th>
						<th scope="col" class="text-center">Role</th>
						<th scope="col" class="text-center">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$sql = "SELECT * FROM tbl_user";
					$query = mysqli_query($db, $sql);
					$no = 1;

					while ($data = mysqli_fetch_array($query)) {
					?>
						<tr>
							<td class='text-center'><?php echo $data['nama'] ?></td>
							<td class='text-center'><?php echo $data['nip'] ?></td>
							<td class='text-center'><?php echo $data['tempatLahir'] ?></td>
							<td class='text-center'><?php echo $data['tanggalLahir'] ?></td>
							<td class='text-center'><?php echo $data['pangkat'] ?></td>
							<td class='text-center'><?php echo $data['jabatan'] ?></td>
							<td class='text-center'><?php echo $data['role'] ?></td>

							<td class='align-middle'>
								<div class='d-flex justify-content-center'>
									<button type="button" class="btn btn-outline-warning btn-sm" data-bs-toggle="modal" data-bs-target="#editData<?php echo $no ?>">Edit</button>&nbsp
									<button type="button" class="btn btn-outline-danger btn-sm" data-bs-toggle="modal" data-bs-target="#hapusData<?php echo $no ?>">Hapus</button>
								</div>
							</td>
							<div class="modal fade" id="editData<?php echo $no ?>" tabindex="-1" aria-labelledby="edittambahModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="edittambahModalLabel">Edit Data</h5>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<form action="confAdmin.php" method="POST">
											<div class="modal-body">
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Nama</label>
													<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $data['nama'] ?>" name="nama">
													<input type="hidden" class="form-control" id="exampleFormControlInput1" value="<?php echo $data['id'] ?>" name="id">
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">NIP</label>
													<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan NIP" value="<?php echo $data['nip'] ?>" name="nip">
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Tempat Lahir</label>
													<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tempat Lahir" value="<?php echo $data['tempatLahir'] ?>" name="tempatLahir">
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Tanggal Lahir</label>
													<input type="date" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Tanggal Lahir" value="<?php echo $data['tanggalLahir'] ?>" name="tanggalLahir">
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Pangkat</label>
													<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Pangkat" value="<?php echo $data['pangkat'] ?>" name="pangkat">
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Jabatan</label>
													<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Jabatan" value="<?php echo $data['jabatan'] ?>" name="jabatan">
												</div>
												<div class="mb-3">
													<label for="recipient-name" class="col-form-label">Role</label>
													<select class="form-select" aria-label="Default select example" name="role" required>
														<option value="<?php echo $data['role'] ?>"><?php echo $data['role'] ?></option>
														<option value="KMP Full">KMP Full</option>
														<option value="KMP Only">KMP Only</option>
														<option value="UKM Full">UKM Full</option>
														<option value="UKM Only">UKM Only</option>
														<option value="UKPP Full">UKPP Full</option>
														<option value="UKPP Only">UKPP Only</option>
														<option value="PPN Full">PPN Full</option>
														<option value="PPN Only">PPN Only</option>
														<option value="PMP Full">PMP Full</option>
														<option value="PMP Only">PMP Only</option>
														<option value="Kepegawaian">Kepegawaian</option>
														<option value="Admin">Admin</option>
													</select>
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Username</label>
													<input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Username" value="<?php echo $data['username'] ?>" name="username">
												</div>
												<div class="mb-3">
													<label for="exampleFormControlInput1" class="form-label">Password (KOSONGKAN JIKA TIDAK INGIN MENGGANTI PASSWORD)</label>
													<input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Password" name="password">
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
												<button type="submit" class="btn btn-primary" value="edit" name="edit">Simpan Perubahan</button>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="modal fade" id="hapusData<?php echo $no ?>" tabindex="-1" aria-labelledby="hapusModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="hapusModalLabel">Hapus</h5>
											<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
										</div>
										<form action="confAdmin.php" method="POST">
											<div class="modal-body">
												<input type="hidden" class="form-control" id="exampleFormControlInput1" placeholder="Masukkan Nama" value="<?php echo $data['id'] ?>" name="id">
												Apakah anda yakin ingin menghapus data ini?
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
												<button type="submit" class="btn btn-danger" name="hapus" value="hapus">Hapus</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</tr>
					<?php
						$no++;
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- end content -->

	<!-- start footer -->
	<div class="footer text-center text-lg-start fixed-bottom">
		<div class="text-center p-3">Copyright © <script>document.write(new Date().getFullYear())</script> Allrights reserved to Puskesmas Jatilawang
		</div>
	</div>
	<!-- end footer -->

	
</body>
<script src="https://kit.fontawesome.com/412f3cd995.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.29/dist/sweetalert2.all.min.js"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

<script type="text/javascript">
	$(function() {
		$('#tbl_user').DataTable();
	});
</script>

</html>