-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2023 at 06:17 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pkm_jtl`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kmp`
--

CREATE TABLE `tbl_kmp` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `kategori` varchar(244) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp(),
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kmp`
--

INSERT INTO `tbl_kmp` (`id`, `nama`, `kategori`, `tipe`, `waktu`, `namaFile`) VALUES
('2ei10zbfudgksk', 'kmp_full', 'Program MFK', 'pdf', '2022-11-12 06:00:19', '2ei10zbfudgkskProgram MFK29024-75676591818-2-PB.pdf'),
('3l0g2yd2s7wggc', 'kmp_full', 'Struktur Organisasi', 'pdf', '2022-11-12 05:49:04', '3l0g2yd2s7wggcStruktur Organisasi29024-75676591818-2-PB.pdf'),
('4o51oi6d0xs00k', 'kmp_full', 'Ketersediaan SDM', 'pdf', '2022-11-12 05:54:30', '4o51oi6d0xs00kKetersediaan SDM29024-75676591818-2-PB.pdf'),
('4uyidwe9u2yo8s', 'kmp_full', 'Indikator Kinerja', 'pdf', '2022-11-12 06:03:53', '4uyidwe9u2yo8sIndikator Kinerja29024-75676591818-2-PB.pdf'),
('79m9mngmks0sk', 'kmp_full', 'Penetapan Jenis Pelayanan', 'pdf', '2022-11-12 05:31:20', '79m9mngmks0skPenetapan Jenis Pelayanan29024-75676591818-2-PB.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pmp`
--

CREATE TABLE `tbl_pmp` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `kategori` varchar(244) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp(),
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_pmp`
--

INSERT INTO `tbl_pmp` (`id`, `nama`, `kategori`, `tipe`, `waktu`, `namaFile`) VALUES
('4bwui6u8u668og', 'pmp_full', 'Program PPI', 'pdf', '2022-11-12 14:03:10', '4bwui6u8u668ogProgram PPI29024-75676591818-2-PB.pdf'),
('4wchby8jfnacoo', 'pmp_full', 'Identifikasi Resiko Penyelengaraan Upaya Puskesmas', 'pdf', '2022-11-12 13:57:14', '4wchby8jfnacooIdentifikasi Resiko Penyelengaraan Upaya Puskesmas29024-75676591818-2-PB.pdf'),
('4zb8d9e4qo000s', 'pmp_full', 'Identifikasi Pasien dengan Benar', 'pdf', '2022-11-12 13:59:30', '4zb8d9e4qo000sIdentifikasi Pasien dengan Benar29024-75676591818-2-PB.pdf'),
('w4jk07mevpc0g', 'pmp_full', 'Peningkatan Mutu Puskesmas Oleh Tim Mutu', 'pdf', '2022-11-12 13:55:17', 'w4jk07mevpc0gPeningkatan Mutu Puskesmas Oleh Tim Mutu29024-75676591818-2-PB.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ppn`
--

CREATE TABLE `tbl_ppn` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `kategori` varchar(244) NOT NULL,
  `tipe` varchar(100) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp(),
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_ppn`
--

INSERT INTO `tbl_ppn` (`id`, `nama`, `kategori`, `tipe`, `waktu`, `namaFile`) VALUES
('21o0ubsskuu8gk', 'ppn_full', 'Program Pengendalian Penyakit Tidak Menular dan Faktor Resikonya', 'pdf', '2022-11-12 13:14:35', '21o0ubsskuu8gkProgram Pengendalian Penyakit Tidak Menular dan Faktor Resikonya29024-75676591818-2-PB.pdf'),
('48rnpawoi50k4g', 'ppn_full', 'Pelayanan Kesehatan Ibu Hamil dan Bersalin', 'pdf', '2022-11-12 13:09:40', '48rnpawoi50k4gPelayanan Kesehatan Ibu Hamil dan Bersalin29024-75676591818-2-PB.pdf'),
('4ejrvh3c2aeckk', 'ppn_full', 'Pelaksanaan Program Imunisasi', 'pdf', '2022-11-12 13:10:42', '4ejrvh3c2aeckkPelaksanaan Program Imunisasi29024-75676591818-2-PB.pdf'),
('4n3zm5k1zgcgoc', 'ppn_full', 'Pemantauan Pencegahan dan Penurunan Stunting', 'pdf', '2022-11-12 13:15:54', '4n3zm5k1zgcgoc29024-75676591818-2-PB.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ukm`
--

CREATE TABLE `tbl_ukm` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kategori` text NOT NULL,
  `tipe` varchar(50) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp(),
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_ukm`
--

INSERT INTO `tbl_ukm` (`id`, `nama`, `kategori`, `tipe`, `waktu`, `namaFile`) VALUES
('1z6nu02tr7i84g', 'ukm_full', 'Penjadwalan Pelayanan UKM', 'pdf', '2022-11-12 06:59:10', '1z6nu02tr7i84gPenjadwalan Pelayanan UKM29024-75676591818-2-PB.pdf'),
('2mrnmxvsvl44s8', 'ukm_full', 'Capaian Tujuan Koordinator UKM', 'pdf', '2022-11-12 07:33:50', '2mrnmxvsvl44s8Capaian Tujuan Koordinator UKM29024-75676591818-2-PB.pdf'),
('3tpwjk8dmcis4w', 'ukm_full', 'Cakupan Pelaksanaan UKM Pengembangan', 'pdf', '2022-11-12 07:39:08', '3tpwjk8dmcis4wCakupan Pelaksanaan UKM Pengembangan29024-75676591818-2-PB.pdf'),
('4esf8k7tbm68wg', 'ukm_full', 'Perencanaan UKM Berbasis Wilayah', 'pdf', '2022-11-12 06:57:06', '4esf8k7tbm68wgPerencanaan UKM Berbasis Wilayah29024-75676591818-2-PB.pdf'),
('5behecpo5mkgcs', 'ukm_full', 'Supervisi Penanggung Jawab UKM', 'pdf', '2022-11-12 07:41:25', '5behecpo5mkgcsSupervisi Penanggung Jawab UKM29024-75676591818-2-PB.pdf'),
('dbzw7m8xyjccg', 'ukm_full', 'Penyelenggaraan Pelayanan UKM Puskesmas', 'pdf', '2022-11-12 07:00:48', 'dbzw7m8xyjccgPenyelenggaraan Pelayanan UKM Puskesmas29024-75676591818-2-PB.pdf'),
('pk437ouyxcgcg', 'ukm_full', 'Cakupan Pelaksanaan UKM Esensial Promosi Kesehatan', 'pdf', '2022-11-12 07:37:46', 'pk437ouyxcgcgCakupan Pelaksanaan UKM Esensial Promosi Kesehatan29024-75676591818-2-PB.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ukpp`
--

CREATE TABLE `tbl_ukpp` (
  `id` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kategori` text NOT NULL,
  `tipe` varchar(20) NOT NULL,
  `waktu` timestamp NOT NULL DEFAULT current_timestamp(),
  `namaFile` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_ukpp`
--

INSERT INTO `tbl_ukpp` (`id`, `nama`, `kategori`, `tipe`, `waktu`, `namaFile`) VALUES
('1h3n7skmofwgs0', 'ukpp_full', 'Pemberian Terapi Makanan dan Gizi', 'pdf', '2022-11-12 10:19:18', '1h3n7skmofwgs0Pemberian Terapi Makanan dan Gizi29024-75676591818-2-PB.pdf'),
('2detqprhrri8k8', 'ukpp_full', 'Prioritas Pasien Gawat Darurat', 'pdf', '2022-11-12 10:16:20', '2detqprhrri8k8Prioritas Pasien Gawat Darurat29024-75676591818-2-PB.pdf'),
('2xu0o533iaqs4w', 'ukpp_full', 'Pelayanan Laboratorium Sesuai Prosedur', 'pdf', '2022-11-12 10:24:26', '2xu0o533iaqs4wPelayanan Laboratorium Sesuai Prosedur29024-75676591818-2-PB.pdf'),
('2zcerxmoxaio80', 'ukpp_full', 'Pelayanan Kebutuhan Pasien', 'pdf', '2022-11-12 10:09:21', '2zcerxmoxaio80Pelayanan Kebutuhan Pasien29024-75676591818-2-PB.pdf'),
('32ae27my0lico4', 'ukpp_full', 'Tata Kelola Penyelenggaraan Rekam Medis', 'pdf', '2022-11-12 10:23:17', '32ae27my0lico4Tata Kelola Penyelenggaraan Rekam Medis29024-75676591818-2-PB.pdf'),
('3odl89lht6w4kw', 'ukpp_full', 'Pemulangan Pasien Dipandu Prosedur', 'pdf', '2022-11-12 10:20:25', '3odl89lht6w4kwPemulangan Pasien Dipandu Prosedur29024-75676591818-2-PB.pdf'),
('42y10u6ikhusco', 'ukpp_full', 'Pelayanan Anestasi Lokal', 'pdf', '2022-11-12 10:17:56', '42y10u6ikhuscoPelayanan Anestasi Lokal29024-75676591818-2-PB.pdf'),
('48mm45qp6ry80w', 'ukpp_full', 'Pelaksanaan Rujukan', 'pdf', '2022-11-12 10:22:00', '48mm45qp6ry80wPelaksanaan Rujukan29024-75676591818-2-PB.pdf'),
('5empxdjy4w84wo', 'ukpp_full', 'Skrinning dan Proses Kajian Awal Dilakukan Secara Paripurna', 'pdf', '2022-11-12 10:15:01', '5empxdjy4w84woSkrinning dan Proses Kajian Awal Dilakukan Secara Paripurna29024-75676591818-2-PB.pdf'),
('5k7a72vdfcowgk', 'ukpp_full', 'Pelayanan Kefarmasian Sesuai Prosedur', 'pdf', '2022-11-12 10:25:28', '5k7a72vdfcowgkPelayanan Kefarmasian Sesuai Prosedur29024-75676591818-2-PB.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(50) NOT NULL,
  `nama` varchar(244) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `tempatLahir` varchar(50) NOT NULL,
  `tanggalLahir` date NOT NULL,
  `pangkat` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `username` varchar(244) NOT NULL,
  `password` varchar(244) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `nama`, `nip`, `tempatLahir`, `tanggalLahir`, `pangkat`, `jabatan`, `username`, `password`, `role`) VALUES
(16, 'blackboy', '00000001', 'gatau', '2022-09-09', 'KMP Full', 'KMP Full', 'blackboy', 'd41d8cd98f00b204e9800998ecf8427e', 'KMP Full'),
(17, 'admin', '1111111111', 'Martapura', '2001-03-10', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(20, 'dana', '123123', 'bjb kali', '2009-10-10', 'pengangguran v3', 'pengangguran', '', '', ''),
(21, 'kmp_full', '123321', 'a', '2022-11-11', 'kmp_full', 'kmp_full', 'kmp_full', '80bcd81ff934eb5c893b535d72044a37', 'kmp_full'),
(22, 'kmp_only', '123321123', 'b', '2022-11-08', 'kmp_only', 'kmp_only', 'kmp_only', '8a1a8f8c189ce97430450b8d5f93b984', 'kmp_only'),
(23, 'ukm_full', '1234567876543', 'absdjabsda', '2022-11-02', 'ukm_full', 'ukm_full', 'ukm_full', 'f9e6fbe8d066d00a2689f16bbdc8c702', 'ukm_full'),
(24, 'ukm_only', '567876543', 'gvghvgv', '2022-11-10', 'ukm_only', 'ukm_only', 'ukm_only', '2ba38dd355f9a90458c90ebf2e57b780', 'ukm_only'),
(25, 'ukpp_full', '9823489274', 'sdsfdsfd', '2022-11-10', 'ukpp_full', 'ukpp_full', 'ukpp_full', 'b582b71712bbcd5efeaf4fdef7715253', 'ukpp_full'),
(26, 'ukpp_only', '897865534', 'fsdfsdf', '2022-11-09', 'ukpp_only', 'ukpp_only', 'ukpp_only', '3ef8ac2787125555153971a402b0ab1b', 'ukpp_only'),
(27, 'ppn_full', '0987656789', 'naksjdnaksd', '2022-11-02', 'ppn_full', 'ppn_full', 'ppn_full', '7744e1d372f447dc719f97297d46be64', 'ppn_full'),
(28, 'ppn_only', '92834293', 'ppn_only', '2022-11-10', 'ppn_only', 'ppn_only', 'ppn_only', 'fc4b0dba7143ab4af9b5793426640c26', 'ppn_only'),
(29, 'pmp_full', '5678765', 'sdjfhbsj', '2022-11-02', 'pmp_full', 'pmp_full', 'pmp_full', '6b9583ecbbe06d891d9ecb461637858e', 'pmp_full'),
(30, 'pmp_only', '676545678', 'asjnaksd', '2022-11-02', 'pmp_only', 'pmp_only', 'pmp_only', 'b1c484fe92844c7dc0c361bc7ded08fc', 'pmp_only');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_kmp`
--
ALTER TABLE `tbl_kmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pmp`
--
ALTER TABLE `tbl_pmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ppn`
--
ALTER TABLE `tbl_ppn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ukm`
--
ALTER TABLE `tbl_ukm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ukpp`
--
ALTER TABLE `tbl_ukpp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
